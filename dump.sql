--
-- Скрипт сгенерирован Devart dbForge Studio 2020 for MySQL, Версия 9.0.470.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 18.01.2021 7:31:31
-- Версия сервера: 8.0.19
-- Версия клиента: 4.1
--

--
-- Отключение внешних ключей
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Установить режим SQL (SQL mode)
--
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE test;

--
-- Удалить таблицу `users`
--
DROP TABLE IF EXISTS users;

--
-- Установка базы данных по умолчанию
--
USE test;

--
-- Создать таблицу `users`
--
CREATE TABLE users (
                       id int NOT NULL AUTO_INCREMENT,
                       login varchar(255) NOT NULL,
                       password varchar(255) NOT NULL,
                       fio varchar(255) DEFAULT NULL,
                       email varchar(50) DEFAULT NULL,
                       role tinyint NOT NULL DEFAULT 0,
                       PRIMARY KEY (id)
)
    ENGINE = INNODB,
AUTO_INCREMENT = 22,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Создать индекс `IDX_users` для объекта типа таблица `users`
--
ALTER TABLE users
    ADD INDEX IDX_users (login, password);

--
-- Создать индекс `UK_users_login` для объекта типа таблица `users`
--
ALTER TABLE users
    ADD UNIQUE INDEX UK_users_login (login);

--
-- Вывод данных для таблицы users
--
INSERT INTO users VALUES
(4, 'test ', 'test ', 'test111', 'test ', 0),
(6, 'admin', 'admin', 'admin', 'admin', 1);

--
-- Восстановить предыдущий режим SQL (SQL mode)
--
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

--
-- Включение внешних ключей
--
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;