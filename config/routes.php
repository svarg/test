<?php

use App\Controller\HomeController;

return [
    '/auth' => ['controller' => new HomeController(), 'action' => 'actionAuth', 'method' => 'POST'],
    '/list' => ['controller' => new HomeController(), 'action' => 'actionList', 'method' => 'ANY'],
    '/logout' => ['controller' => new HomeController(), 'action' => 'actionLogout', 'method' => 'ANY'],
    '/update/([0-9]+)' => ['controller' => new HomeController(), 'action' => 'actionUpdate', 'method' => 'ANY'],
    '/updateUser' => ['controller' => new HomeController(), 'action' => 'actionUpdateUser', 'method' => 'POST'],
    '/insertUser' => ['controller' => new HomeController(), 'action' => 'actionInsertUser', 'method' => 'POST'],
    '/insert' => ['controller' => new HomeController(), 'action' => 'actionInsert', 'method' => 'GET'],
    '/delete' => ['controller' => new HomeController(), 'action' => 'actionDelete', 'method' => 'POST'],
    '/view/([0-9]+)' => ['controller' => new HomeController(), 'action' => 'actionView', 'method' => 'ANY'],
    '/' => ['controller' => new HomeController(), 'action' => 'actionIndex', 'method' => 'GET'],
];