<?php


return [
    'app' => [
        'session_save_path' => '/cashe/sessions',
        'view_path' => '/views'
    ],
    'database' => [
        'driver'=>'mysql',
        'host'=>'localhost',
        'port'=>'3306',
        'dbname'=>'test',
        'user'=>'root',
        'password'=>'root'
    ]
];