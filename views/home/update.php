<?php
/**
 * @var $user
 */
?>
<div class="container">
    <form>
        <table>
            <?php foreach ($user as $key => $val): ?>
                <tr class="<?= empty($val['immutable'])?'':"d-none" ?>">
                    <td><?= $val['name'] ?></td>
                    <td><input name="<?= $key ?>" value="<?= $val['val'] ?> "  ></td>

                </tr>
            <?php endforeach; ?>
        </table>
        <input type="submit" value="изменить">
    </form>
</div>
<script>
    $('form').submit(function (event) {
        // cancels the form submission
        event.preventDefault();
        submitForm($(this));
    });

    function submitForm(form) {
        // Initiate Variables With Form Content
        jQuery.ajax({
            type: "POST",
            url: "/updateUser",
            data: form.serialize(),
            success: function (data) {
                if (data['status'] == 'true') {
                    window.location.replace(data['link'])
                }
            }
        });
    }
</script>