<?php
/**
 * @var $user
 */
?>
<div class="container">
    <form>
        <table>
            <?php foreach ($user as $key => $val): ?>
                <tr class="<?= empty($val['immutable'])?'':"d-none" ?>">
                    <td><?= $val['name'] ?></td>
                    <td><input name="<?= $key ?>" value=""  ></td>

                </tr>
            <?php endforeach; ?>
        </table>
        <input type="submit" value="Добавить">
    </form>
</div>
<script>
    $('form').submit(function (event) {
        // cancels the form submission
        event.preventDefault();
        submitForm($(this));
    });

    function submitForm(form) {
        // Initiate Variables With Form Content
        jQuery.ajax({
            type: "POST",
            url: "/insertUser",
            data: form.serialize(),
            success: function (data) {
                if (data['status'] == 'true') {
                    window.location.replace(data['link'])
                }
            }
        });
    }
</script>