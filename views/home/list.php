<?php
/**
 * @var $users \App\Models\Users
 * @var $role int //0-user,1-admin
 * @var $usersColumn
 * @var $userId //action user id
 * @var $sort //action sort
 */
//var_dump($users);
// сортировки и фильтрации по логину, ФИО и роли.

?>
<div class="container">
    <form>
        <input type="hidden" name="sort" id="sort">
        <table>
            <tr>
                <th id="filter-id"><input type="text" name="filter[id]" value="<?= $filter['id'] ?? '' ?>"></th>
                <th id="filter-login"><input type="text" name="filter[login]" value="<?= $filter['login'] ?? '' ?>">
                </th>
                <th id="filter-fio"><input type="text" name="filter[fio]" value="<?= $filter['fio'] ?? '' ?>"></th>
                <th id="filter-email"><input type="text" name="filter[email]" value="<?= $filter['email'] ?? '' ?>">
                </th>
                <th id="filter-role">
                    <select name="filter[role]">
                        <option value="">-</option>
                        <option value="1">Admin</option>
                        <option value="0">User</option>
                    </select>
                </th>
                <th><input type="submit" value="Применить"></th>

            </tr>
            <tr>
                <th class="sort <?=isset($sort['id'])?'action '.$sort['id']:''?>" id="sort-id" data-field="id">#</th>
                <th class="sort <?=isset($sort['login'])?'action '.$sort['login']:''?>" id="sort-login" data-field="login">Login</th>
                <th class="sort <?=isset($sort['fio'])?'action '.$sort['fio']:''?>" id="sort-fio" data-field="fio">FIO</th>
                <th class="sort <?=isset($sort['email'])?'action '.$sort['email']:''?>" id="sort-email" data-field="email">Email</th>
                <th class="sort <?=isset($sort['role'])?'action '.$sort['role']:''?>" id="sort-role" data-field="role">Role</th>
                <th></th>
            </tr>
            <?php foreach ($users as $user): ?>
                <?php $attributes = $user->getAttributes(); ?>
                <tr>
                    <td><?= $attributes['id']['val'] ?></td>
                    <td><?= $attributes['login']['val'] ?></td>
                    <td><?= $attributes['fio']['val'] ?></td>
                    <td><?= $attributes['email']['val'] ?></td>
                    <td><?= $attributes['role']['val'] ?></td>
                    <?php if ($role == 1): ?>
                        <td>
                            <a class="btn btn-secondary" href="/view/<?= $attributes['id']['val'] ?>">
                                view
                            </a>
                            <a class="btn btn-secondary" href="/update/<?= $attributes['id']['val'] ?>">
                                update
                            </a>
                            <button class="delete" data-id="<?= $attributes['id']['val'] ?>">delete</button>
                        </td>
                    <?php else: ?>
                        <td>
                            <a class="btn btn-secondary" href="/view/<?= $attributes['id']['val'] ?>">
                                view
                            </a>
                            <?php if ($userId == $attributes['id']['val']): ?>
                                <a class="btn btn-secondary" href="/update/<?= $attributes['id']['val'] ?>">
                                    update
                                </a>
                            <?php endif; ?>

                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </form>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {

        $(".delete").on('click', function (event) {
            event.preventDefault();
            id = $(this).data('id');
            jQuery.ajax({
                type: "POST",
                url: "/delete",
                data: "id=" + id,
                success: function (data) {
                    if (data['status'] == 'true') {
                        location.reload();
                    }
                }
            });
        })

        $(".sort").on('click', function (event) {
console.log($(this).hasClass("action"));
            if ($(this).hasClass("action")) {
                if($(this).hasClass("ASC")){
                    $(".sort").removeClass("ASC")
                    $(this).addClass("DESC");
                    $("input#sort").val("DESC");
                }else {
                    $(".sort").removeClass("DESC")
                    $(this).addClass("ASC");
                    $("input#sort").val("ASC");
                }
                $("input#sort").attr("name","sort["+$(this).data("field")+"]");
            } else {
                $(".sort").removeClass("action");
                $(this).addClass("action");
                $("input#sort").val("ASC");
                $("input#sort").attr("name","sort["+$(this).data("field")+"]");
            }
             $("form").submit();
        })
    })
</script>
<style>
    .action {
        color: #00F;
        text-decoration: underline;
    }

    .action.ASC::before {
        content: "ᐁ";
    }

    .action.DESC::before {
        content: "ᐃ";
    }

</style>
