<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 11.01.2021
 * Time: 23:01
 */
?>
<div class="container">
    <form>
        <div class="form-group">
            <label for="login">Email address</label>
            <input type="text" class="form-control" id="login" name="login" placeholder="Enter login">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <button type="submit" id="form-submit" class="btn btn-success btn-lg pull-right ">Submit</button>
    </form>

</div>
<script>
    document.addEventListener('DOMContentLoaded', function(){

        $('form').submit(function (event) {
            // cancels the form submission
            event.preventDefault();
            submitForm($(this));
        });

        function submitForm(form) {
            // Initiate Variables With Form Content
            jQuery.ajax({
                type: "POST",
                url: "/auth",
                data: form.serialize(),
                success: function (data) {
                    if (data['status']=='true') {
                        window.location.replace(data['link'])
                    }
                }
            });
        }
    })
</script>




