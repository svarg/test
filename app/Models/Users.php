<?php


namespace App\Models;


use Core\Db\Model;

class Users extends Model
{
    protected int $id;
    protected string $login;
    protected string $password;
    protected string $fio;
    protected string $email;
    protected string $role;

    protected static function table()
    {
        return 'users';
    }

    public static function allUsers()
    {
        return self::query()->all();
    }

    public static function authUser($login, $password)
    {
        return self::query()
            ->where('login', '=', $login)
            ->where('password', '=', $password)
            ->get();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAttributes()
    {
        $this->attributes = [
            'id' => ['val' => $this->id, 'name' => '#', 'immutable'=>1],
            'login' => ['val' => $this->login, 'name' => 'Логин'],
            'password' => ['val' => $this->login, 'name' => 'Пароль'],
            'fio' => ['val' => $this->fio, 'name' => 'ФИО'],
            'email' => ['val' => $this->email, 'name' => 'E-mail'],
            'role' => ['val' => $this->role, 'name' => 'Право пользователя'],
        ];
        return $this->attributes;
    }


    public function getPassword()
    {
        return $this->password;
    }

    public function getRole(): int
    {
        return $this->role;
    }
}