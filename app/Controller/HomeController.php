<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 11.01.2021
 * Time: 3:29
 */

namespace App\Controller;


use App\Models\Users;
use Core\Http\{Request, Response};
use Core\View;

class HomeController
{
    public function actionIndex(Request $request, Response $response)
    {
        return $response->send(View::render('home.index'));
    }

    public function actionAuth(Request $request, Response $response)
    {

        $user = Users::authUser($request->post()['login'], $request->post()['password']);
        if (empty($user)) {
            $json = json_encode(['status' => 'false']);
        } else {
            $_SESSION['authorize'] = true;
            $_SESSION['user_id'] = $user->getId();
            $json = json_encode(['status' => 'true', 'link' => '/list']);
        }
        return $response->headers('Content-Type', 'application/json')->send($json);
    }

    public function actionList(Request $request, Response $response)
    {
        $user_id = $this->auth($response);
        if (empty($user_id)) {
            return $response->headers("Location", "/")->status(302);
        }
        $user = Users::query()->where('id', '=', $user_id)->get();
        if (empty($request->get()['filter'])) {
            $data['users'] = Users::allUsers();
        } else {
            $userQuery = Users::query();
            foreach ($request->get()['filter'] as $key => $value) {
                if (!empty($value)) {
                    $userQuery->where($key, " LIKE ", "%$value%");
                }
            }
            if (!empty($request->get()['sort'])) {

                foreach ($request->get()['sort'] as $key => $value) {
                    if (!empty($value)) {
                        $userQuery->orderBy($key, $value);
                        $data['sort'][$key] = $value;
                    }
                }
            }
            $data['users'] = $userQuery->all();
            $data['filter'] = $request->get()['filter'];
        }
        $data['role'] = $user->getRole();
        $data['userId'] = $user->getId();

        return $response->send(View::render('home.list', $data));
    }

    public function actionDelete(Request $request, Response $response)
    {
        $user_id = $this->auth($response);
        if (empty($user_id)) {
            return $response->headers("Location", "/")->status(302);
        }
        Users::query()->where('id', '=', $request->post()['id'])->delete();
        $json = json_encode(['status' => 'true', 'link' => '/list']);
        return $response->headers('Content-Type', 'application/json')->send($json);
    }

    public function actionInsert(Request $request, Response $response)
    {
        $user_id = $this->auth($response);
        if (empty($user_id)) {
            return $response->headers("Location", "/")->status(302);
        }
        $actionUser = Users::query()->where('id', '=', $user_id)->get();
        $data['role'] = $actionUser->getRole();
        if ($data['role'] == 1) {
            $data['user'] = $actionUser->getAttributes();
            unset($data['user']['id']);
            return $response->send(View::render('home.insert', $data));
        } else {
            return $response->headers("Location", "/list")->status(302);
        }
    }

    public function actionUpdate(Request $request, Response $response)
    {
        $user_id = $this->auth($response);
        if (empty($user_id)) {
            return $response->headers("Location", "/")->status(302);
        }
        $user = Users::query()->where('id', '=', $request->get()['id'])->get();
        $actionUser = Users::query()->where('id', '=', $user_id)->get();
        if (empty($user)) {
            throw new  \Exception("404 no found");
        }
        $data['role'] = $actionUser->getRole();
        if ($user->getId() == $user_id || $data['role'] == 1) {
            $data['user'] = $user->getAttributes();
            if ($data['role'] != 1) {
                unset($data['user']['role']);
            }
            return $response->send(View::render('home.update', $data));
        } else {
            return $response->headers("Location", "/list")->status(302);
        }
    }


    public function actionUpdateUser(Request $request, Response $response)
    {
        $user_id = $this->auth($response);
        if (empty($user_id)) {
            return $response->headers("Location", "/")->status(302);
        }
        if (empty(Users::query()->where('id', '=', $request->post()['id'])->update($request->post()))) {
            throw new \ErrorException("500 error update user");

        }
        $json = json_encode(['status' => 'true', 'link' => '/list']);
        return $response->headers('Content-Type', 'application/json')->send($json);
    }

    public function actionInsertUser(Request $request, Response $response)
    {
        $user_id = $this->auth($response);
        if (empty($user_id)) {
            return $response->headers("Location", "/")->status(302);
        }
        $user = Users::query()->insert($request->post());
        if (empty($user)) {
            throw new \ErrorException("500 error insert user");

        }
        $json = json_encode(['status' => 'true', 'link' => '/list']);
        return $response->headers('Content-Type', 'application/json')->send($json);
    }

    public function actionView(Request $request, Response $response)
    {
        $user_id = $this->auth($response);
        if (empty($user_id)) {
            return $response->headers("Location", "/")->status(302);
        }
        $user = Users::query()->where('id', '=', $request->get()['id'])->get();
        if (empty($user)) {
            throw new  \Exception("404 no found");
        }
        $data['user'] = $user->getAttributes();
        return $response->send(View::render('home.view', $data));
    }

    public function actionLogout(Request $request, Response $response)
    {
        $_SESSION['authorize'] = false;
        unset($_SESSION['user_id']);
        return $response->headers("Location", "/")->status(302);
    }

    protected function auth(Response $response)
    {
        if ($_SESSION['authorize'] == false && empty($_SESSION['user_id'])) {
            return false;
        }
        return $_SESSION['user_id'];
    }


}