<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 11.01.2021
 * Time: 22:38
 */

namespace Core;


class View
{
    public static string $layout = 'layouts.main';
    protected static $viewPath;

    public static function init(string $viewPath)
    {
        self::$viewPath = $viewPath;
    }

    public static function render(string $fileName, array $data = [])
    {
        return self::renderContent(self::renderFile($fileName, $data));
    }

    public static function renderContent($content)
    {
        return self::renderFile(self::$layout, ['content' => $content]);
    }

    protected static function renderFile(string $fileName, array $data = []): string
    {
        $pathParts = explode('.', $fileName);
        $filePath = self::$viewPath . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $pathParts) . '.php';
        ob_start();
        ob_implicit_flush(false);
        extract($data);
        require $filePath;
        return ob_get_clean();
    }


}