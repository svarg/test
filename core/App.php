<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 11.01.2021
 * Time: 1:17
 */

namespace Core;


use Core\Http\Request;
use Core\Http\Response;
use Core\Http\Router;
use Core\Session\SessionHandler;

class App
{
    protected static $root;

    protected $configPath = '';
    protected $routesPath = '';
    protected $request = null;

    protected static $config = [];

    public function __construct()
    {
        self::$root = dirname(getcwd());
        $this->configPath = self::$root . "/config/config.php";
        $this->routesPath = self::$root . "/config/routes.php";
    }

    /**
     * @param string $configPath
     * @return App
     */
    public function setConfigPath($configPath)
    {
        $this->configPath = self::$root . $configPath;
        return $this;
    }

    /**
     * @param string $routesPath
     * @return App
     */
    public function setRoutesPath($routesPath)
    {
        $this->routesPath = self::$root . $routesPath;
        return $this;
    }

    public function run()
    {
        /**
         * @var Response $response
         */
        $response = $this->init()
            ->startSession()
            ->dispatch();

        $this->terminate($response);
    }

    protected function init()
    {
        self::$config = include_once $this->configPath;

        $this->request = new Request();
        Router::setRoutes($this->routesPath);
        View::init(self::$root . self::$config['app']['view_path']);
        set_error_handler([ErrorHandler::class, 'error']);
        set_exception_handler([ErrorHandler::class, "exception"]);
        return $this;
    }

    public function startSession()
    {
        session_save_path(self::$root . self::$config['app']['session_save_path']);
        session_set_save_handler(new SessionHandler());
        session_start();
        $_SESSION['authorize'] ??= false;
        return $this;
    }

    protected function dispatch()
    {
        return Router::dispatch($this->request);
    }

    protected function terminate(Response $response)
    {
        foreach ($response->getHeaders() as $header => $value) {
            header("$header:$value");
        }
        http_response_code($response->getStatus());
        exit($response->getContent());
    }

    public static function config()
    {
        return self::$config;
    }
}