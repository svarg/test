<?php


namespace Core\Db;


class Model
{
    protected array $attributes = [];

    protected static function table()
    {
        return 'table';
    }

    public static function query()
    {
        $query = new QueryBuild();
        return $query->table(static::table())->model(static::class);
    }

    public function __set($name, $value)
    {

    }


}