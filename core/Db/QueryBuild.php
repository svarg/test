<?php


namespace Core\Db;


class QueryBuild
{
    protected Connect $connect;

    protected string $table = '';
    protected array $selects = ['*'];
    protected array $wheres = [];
    protected array $sorts = [];
    protected int $limit = 0;

    public function __construct()
    {
        $this->connect = new Connect();
    }

    /**
     * @param string $table
     * @return QueryBuild
     */
    public function table(string $table): QueryBuild
    {
        $this->table = $table;
        return $this;
    }

    public function model(string $model)
    {
        $this->connect->setFetchMode([Connect::AS_OBJECT, $model]);
        return $this;
    }

    /**
     * @param array $selects
     * @return QueryBuild
     */
    public function select(array $selects = ['*']): QueryBuild
    {
        $this->selects = $selects;
        return $this;
    }

    /**
     * @param string $wheres
     * @param $op
     * @param $value
     * @return $this
     */
    public function where(string $field, $op, $value): QueryBuild
    {
        if (is_null($value)) {
            [$op, $value] = ['=', $op];
        }
        $this->wheres[] = [[$field, $op], $value];
        return $this;
    }

    /**
     * @param string $field
     * @param $parametr
     * @return $this
     */
    public function orderBy(string $field, $parametr)
    {
        $this->sorts[] = $field . " " . $parametr;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function limit(int $limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function toSql()
    {
        $sql = "SELECT " . implode(',', $this->selects) . " FROM " . $this->table . " ";
        if (count($this->wheres)) {
            $sql .= " WHERE ";
            $sql .= implode(' AND ', array_map(fn($where) => "{$where[0]} {$where[1]} ?", array_column($this->wheres, 0)));
        }
        if (count($this->sorts)) {
            $sql .= " ORDER BY ";
            $sql .= implode(',', $this->sorts);
        }
        if (!empty($this->limit)) {
            $sql .= " LIMIT " . $this->limit;
        }
        return $sql;
    }

    protected function getWhereValues()
    {
        return array_column($this->wheres, 1);
    }

    public function get()
    {
        return $this->connect->fetch($this->toSql(), $this->getWhereValues());
    }

    public function all()
    {
        return $this->connect->fetchAll($this->toSql(), $this->getWhereValues());
    }

    public function delete()
    {
        $sql = "DELETE FROM " . $this->table;
        if (count($this->wheres)) {
            $sql .= " WHERE ";
            $sql .= implode(' AND ', array_map(fn($where) => "{$where[0]} {$where[1]} ?", array_column($this->wheres, 0)));
        }
        return $this->connect->delete($sql, $this->getWhereValues());
    }

    public function update(array $attributes)
    {
        $sql = "UPDATE " . $this->table;
        $sql .= " SET ";
        $sql .= implode(',', array_map(fn($attribute) => "$attribute  = :$attribute", array_keys($attributes),));
        if (count($this->wheres)) {
            $sql .= " WHERE ";
            $sql .= implode(' AND ', array_map(fn($where) => "{$where[0]} {$where[1]} :{$where[0]}", array_column($this->wheres, 0)));
        }
        return $this->connect->update($sql, $attributes);
    }

    public function insert(array $attributes)
    {
        $sql = "INSERT INTO " . $this->table;
        $sql.=" (";
        $sql .= implode(',', array_map(fn($attribute) => $attribute , array_keys($attributes),));
        $sql.=" )";
        $sql.=" VALUES (";
        $sql .= implode(',', array_map(fn($attribute) => ":$attribute" , array_keys($attributes),));
        $sql.=")";
        return $this->connect->insert($sql, $attributes);
    }

    public function getColumn()
    {
        return $this->connect->fetchColumn("DESCRIBE " . $this->table);
    }


}