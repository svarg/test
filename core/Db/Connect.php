<?php


namespace Core\Db;


use Core\App;
use PDO;

class Connect
{
    public const AS_ARRAY = PDO::FETCH_ASSOC;
    public const AS_OBJECT = PDO::FETCH_CLASS;

    protected const AVAIBLE_MODELS = [self::AS_ARRAY, self::AS_OBJECT];

    protected PDO $pdo;
    protected array $fetchMode = [self::AS_ARRAY];

    public function __construct()
    {
        $database = App::config()['database'];
        $connect = "{$database['driver']}:host={$database['host']};port={$database['port']};dbname={$database['dbname']}";
        $this->pdo = new PDO($connect, $database['user'], $database['password']);
    }

    public function setFetchMode(array $fetchMode)
    {
        $this->fetchMode = $fetchMode;
        return $this;
    }

    /**
     * @param string $sql
     * @param array $params
     */
    protected function execute(string $sql, array $params)
    {
        $statement = $this->pdo->prepare($sql);
        $statement->setFetchMode(...$this->fetchMode);
        $statement->execute($params);
        return $statement;
    }

    public function fetch(string $sql, array $params)
    {
       return $this->execute($sql, $params)->fetch();
    }

    public function fetchAll(string $sql, array $params)
    {
        return $this->execute($sql, $params)->fetchAll();
    }

    public function update(string $sql,array $params)
    {
        $sth = $this->pdo->prepare($sql);
        $sth->execute($params);
        return $sth->rowCount();
    }

    public function delete(string $sql, array $params)
    {
        $sth = $this->pdo->prepare($sql);
        $sth->execute($params);
        return $sth->rowCount();
    }

    public function insert(string $sql, array $params)
    {
        return $this->execute($sql,$params)->rowCount();
    }

    public function fetchColumn(string $sql)
    {
        $sth = $this->pdo->prepare($sql);
        $sth->execute();
        $ar = $sth->fetchAll(PDO::FETCH_COLUMN);
        return $ar;
    }

    public function fetchLazy(string $sql, array $params)
    {
        $statement = $this->execute($sql, $params);
        while ($row = $statement->fetch()) {
            yield $row;
        }

    }
}