<?php
namespace Core;


class ErrorHandler
{
    public static function error($code, $message, $filename, $line)
    {
        throw new \ErrorException($message, $code, 1, $filename, $line);
    }

    public static function exception(\Throwable $exception)
    {
        exit("<pre>" . $exception . "</pre>");
    }

}