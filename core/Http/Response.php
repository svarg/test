<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 11.01.2021
 * Time: 23:16
 */

namespace Core\Http;


class Response
{
    protected $headers = [];
    protected $status = 200;
    protected $content = '';

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $header
     * @param array $value
     * @return $this
     */
    public function headers(string $header, string $value): Response
    {
        $this->headers[$header] = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function status(int $status): Response
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function send(string $content): Response
    {
        $this->content = $content;
        return $this;
    }
}