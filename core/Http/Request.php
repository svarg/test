<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 11.01.2021
 * Time: 2:43
 */

namespace Core\Http;


class Request
{
    protected $get;
    protected $post;
    protected $method;
    protected $path;

    public function __construct()
    {
        $this->get = $_GET;
        $this->post = $_POST;
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->path = $_SERVER['REQUEST_URI'] ?? '/';
    }

    /**
     * @param mixed $get
     */
    public function get()
    {
        return $this->get;
    }

    /**
     * @param mixed $post
     */
    public function post()
    {
        return $this->post;
    }

    /**
     * @param mixed $method
     */
    public function method()
    {
        return $this->method;
    }

    /**
     * @param string $path
     */
    public function path()
    {
        return $this->path;
    }
    public function setGet($key,$val)
    {
        $this->get[$key]=$val;
    }
    public function setPost($key,$val)
    {
        $this->post[$key]=$val;
    }



}