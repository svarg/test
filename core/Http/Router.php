<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 11.01.2021
 * Time: 2:30
 */

namespace Core\Http;

class Router
{
    protected static $routes = [];

    public static function setRoutes(string $routesPath)
    {
        self::$routes = include_once $routesPath;
    }

    /**
     * @param Request $request
     * @return \Core\Http\Response
     * @throws \Exception
     */
    public static function dispatch(Request $request)
    {
        foreach (self::$routes as $key => $val) {
            if (preg_match("~$key~", $request->path())) {
                preg_match("~$key~", $request->path(), $output_array);
                $handler = self::$routes[$key];
                if (isset($output_array[1])) {
                    switch ($request->method()) {
                        case 'POST':
                            $request->setPost('id', $output_array[1]);
                            break;
                        case 'GET':
                            $request->setGet('id', $output_array[1]);
                            break;
                    }
                }
                break;
            }
        }
        if (is_null($handler) || !($request->method() == $handler['method'] || $handler['method'] == 'ANY')) {
            throw new  \Exception("404 no found");
        }
        $response = call_user_func_array([$handler['controller'], $handler['action']], [$request, new Response()]);
        if (!$response instanceof Response) {
            return (new Response())->send($response);
        }

        return $response;
    }

}